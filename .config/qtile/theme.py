from libqtile import layout

colors = {
    'panel_background': "#071529",
    'current_background': "#094963",
    'font': "#ffffff",
    'font_dark': "#292d3e",
    'selection': "#ff5555",
    'layout_focus': "#00caff",
    'border': "#1D2330",
    'purp': "#511539",
    'clock': "#081c42",
    # 'media': "#3EDA79",
    'media': "#20446d",
    'temp': "#217fab",
    'status': "#217fab",
    'dunno': "#FDA400",
    'layout': "#C0C0A2",
    'battery': "#F8467D",
    'systray': "#292d3e",
    'graph_line': '#F8467D'
}

layout_theme = {
    "border_width": 2,
    "border_focus": colors['layout_focus'],
    "border_normal": colors['border']
}

# Ref: http://docs.qtile.org/en/latest/manual/ref/layouts.html#ref-layouts
layouts = [
    layout.Columns(num_columns=2, margin=3, **layout_theme),
    layout.MonadTall(ratio=0.6, max_ratio=0.85, min_ratio=0.15, margin=6,
                     align=layout.MonadTall._right, **layout_theme),
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=1, **layout_theme),
    layout.Floating(**layout_theme)
]

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'gnome-screenshot'},
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'Matplotlib'},
    {'wmclass': 'evolution-alarm-notify'},
    {'wmclass': 'yubioath-desktop'},
    {'wmclass': 'yubikey-personalization-gui'},
    {'wmclass': 'kite'},
])
