import os
import socket

from libqtile import bar, widget

# from keys import my_terminal
from theme import colors

my_terminal = "alacritty"


class ArrowBlock(widget.TextBox):
    """Simple text widget to show left arrow"""

    def __init__(self,
                 background,
                 foreground,
                 text="◢",
                 fontsize=60,
                 padding=0,
                 width=bar.CALCULATED,
                 font='Ubuntu Mono Bold',
                 **config):
        super(ArrowBlock, self).__init__(foreground=foreground,
                                         background=background,
                                         fontsize=fontsize,
                                         padding=padding,
                                         text=text,
                                         font=font,
                                         width=width,
                                         **config)


class FadeBlock(widget.TextBox):
    """Simple text widget to fade colors"""

    def __init__(self,
                 background,
                 foreground,
                 text="░",
                 fontsize=18,
                 padding=0,
                 width=bar.CALCULATED,
                 font='MesloLGS NF Bold',
                 **config):
        super(FadeBlock, self).__init__(foreground=foreground,
                                        background=background,
                                        fontsize=fontsize,
                                        padding=padding,
                                        text=text,
                                        font=font,
                                        width=width,
                                        **config)


class FadeBlockLeft(FadeBlock):
    """Simple text widget to fade colors from the left"""

    def __init__(self, background, foreground, **config):
        text = "▓▒"
        super(FadeBlockLeft, self).__init__(foreground=foreground,
                                            background=background,
                                            text=text,
                                            **config)


class FadeBlockRight(FadeBlock):
    """Simple text widget to fade colors from the right"""

    def __init__(self, background, foreground, **config):
        text = "▒▓"
        super(FadeBlockRight, self).__init__(foreground=foreground,
                                             background=background,
                                             text=text,
                                             **config)


monitor_callback = {'Button1': lambda qtile: qtile.cmd_spawn('htop')},

widget_defaults = dict(font="MesloLGS NF",
                       fontsize=12,
                       padding=0,
                       background=colors['current_background'],
                       foreground=colors['font'])

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())


def get_base_widgets():
    base_widgets_list = [
        # **Left**
        widget.Sep(linewidth=0,
                   padding=6,
                   background=colors['panel_background']),
        widget.Image(filename="~/.config/qtile/icons/sun.png",
                     padding=3,
                     mouse_callbacks={
                         'Button1': lambda qtile: qtile.cmd_spawn('dmenu_run')
                     }),
        widget.GroupBox(font="MesloLGS NF Bold",
                        fontsize=10,
                        margin_y=3,
                        margin_x=0,
                        padding_y=3,
                        padding_x=3,
                        borderwidth=3,
                        active=colors['font'],
                        inactive=colors['font'],
                        rounded=False,
                        highlight_color=colors['current_background'],
                        highlight_method="line",
                        this_current_screen_border=colors['selection'],
                        this_screen_border=colors['purp'],
                        other_current_screen_border=colors['panel_background'],
                        other_screen_border=colors['panel_background'],
                        foreground=colors['font'],
                        background=colors['panel_background']),
        widget.Prompt(prompt=prompt,
                      font="Ubuntu Mono",
                      padding=10,
                      foreground=colors['selection'],
                      background=colors['current_background']),
        widget.Sep(linewidth=0,
                   padding=40,
                   background=colors['panel_background']),
        widget.WindowName(foreground=colors['battery'],
                          background=colors['panel_background']),

        # **Right**
        # widget.TaskList(fmt='', fontsize=0, background=colors['panel_background']),
        widget.Notify(fmt=" 📡 {} ", background=colors['panel_background']),

        # Clock
        ArrowBlock(colors['panel_background'], colors['clock']),
        widget.Clock(background=colors['clock'],
                     foreground=colors['battery'],
                     font="MesloLGS NF Bold",
                     format="%a, %b %d  %H:%M:%S"),
        ArrowBlock(colors['clock'], colors['media']),

        # Media
        widget.Volume(device='pulse', emoji=True, background=colors['media']),
        ArrowBlock(colors['media'], colors['temp']),

        # Temperature
        widget.TextBox(text="🌡 ",
                       padding=0,
                       background=colors['temp'],
                       fontsize=11,
                       font='Ubuntu Mono'),
        widget.ThermalSensor(background=colors['temp'],
                             threshold=90,
                             tag_sensor='Core 0'),
        # FadeBlockRight(colors['temp'], colors['status']),

        # Graphs
        # widget.ImapWidget(server='imap.gmail.com'),
        widget.TextBox(text="  ",
                       background=colors['status'],
                       padding=0,
                       fontsize=14,
                       mouse_callbacks=monitor_callback,
                       font='Ubuntu Mono'),
        widget.MemoryGraph(background=colors['status'],
                           padding=5,
                           graph_color=colors['graph_line'],
                           mouse_callbacks=monitor_callback),
        widget.TextBox(text=" 💻",
                       background=colors['status'],
                       padding=0,
                       fontsize=14,
                       mouse_callbacks=monitor_callback),
        widget.CPUGraph(foreground=colors['font'],
                        background=colors['status'],
                        padding=0,
                        mouse_callbacks=monitor_callback,
                        graph_color=colors['graph_line']),
        widget.TextBox(text="  ",
                       background=colors['status'],
                       padding=0,
                       fontsize=14,
                       font='Ubuntu Mono'),
        widget.NetGraph(foreground=colors['font'],
                        background=colors['status'],
                        graph_color=colors['graph_line'],
                        padding=0),
        # widget.Wlan(foreground=colors['font'], background=colors['status'], padding=2),
        ArrowBlock(colors['status'], colors['purp']),

        # Layout
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            background=colors['purp'],
            padding=0,
            scale=0.7),
        widget.CurrentLayout(foreground=colors['font'],
                             background=colors['purp'],
                             padding=5),
        ArrowBlock(colors['purp'], colors['battery']),

        # Battery
        widget.Battery(foreground=colors['font_dark'],
                       background=colors['battery'],
                       charge_char="",
                       discharge_char="",
                       empty_char="",
                       full_char="",
                       show_short_text=False,
                       padding=5,
                       font='Ubuntu Mono'),
        widget.Sep(linewidth=0, padding=10, background=colors['battery']),

        # Systray
        widget.Systray(background=colors['panel_background'], padding=5),
    ]
    return base_widgets_list
