# -*- coding: utf-8 -*
import os
import socket
import subprocess
from typing import List

from libqtile.config import Key, Screen, Group
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy

import widgets
from groups import groups, group_names
from helpers import bind_group_keys
from keys import keys, mod, mouse
from screens import screens
from theme import colors, floating_layout, layouts


prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
    lazy.restart()


@hook.subscribe.client_new
def floating_dialogs(window):
    dialog = window.window.get_wm_type() == 'dialog'
    transient = window.window.get_wm_transient_for()
    if dialog or transient:
        window.floating = True


if __name__ in ["config", "__main__"]:
    bind_group_keys(group_names, keys)

    widget_defaults = widgets.widget_defaults
    extension_defaults = widget_defaults.copy()
    dgroups_key_binder = None
    dgroups_app_rules = []
    main = None
    follow_mouse_focus = True
    bring_front_click = False
    cursor_warp = False
    auto_fullscreen = True
    focus_on_window_activation = "smart"
    widgets_list = widgets.get_base_widgets()

    wmname = "LG3D"
