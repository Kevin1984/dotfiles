#!/bin/sh
xrandr --output eDP1 --primary --mode 3200x1800 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --off --output HDMI2 --off --output VIRTUAL1 --off &
xrandr --output eDP1 --scale 0.7x0.7
