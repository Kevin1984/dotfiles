from libqtile.config import Group, Match

group_names = [
    (" WWW₁", {'layout': 'columns',
               'spawn': 'firefox'
               }
     ),
    (" CODE₂", {'layout': 'columns',
                'matches': [Match(wm_class='jetbrains-pycharm-ce')],
                }
     ),
    (" MED₃", {'layout': 'stack',
               'matches': [Match(wm_class=['spotify', 'Spotify'])],
               'spawn': 'spt'}),
    (" CHAT₄", {'layout': 'columns',
                 'spawn': ''}),
    (" VID₅", {'layout': 'columns'}),
    (" ₆", {'layout': 'floating'}),
    (" ₇", {'layout': 'columns'}),
    (" ₈", {'layout': 'columns'}),
    (" ₉", {'layout': 'columns'}),
]

groups = [Group(name, **kwargs) for name, kwargs in group_names]
