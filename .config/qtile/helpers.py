from libqtile.config import Key
from libqtile.lazy import lazy

from keys import mod


def bind_group_keys(group_names, keys):
    """Adds bindings based on number of groups"""
    for i, (name, kwargs) in enumerate(group_names, 1):
        if i > 10:
            break
        keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
        keys.append(Key([mod, "shift"], str(i % 10),
                        lazy.window.togroup(name)))  # Send current window to another group
