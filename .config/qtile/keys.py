import os
import subprocess

from libqtile import qtile
from libqtile.config import Key, Drag, Click
from libqtile.lazy import lazy

mod = "mod4"
alt = "mod1"
my_terminal = "alacritty"


def window_to_previous_screen(qtile):
    """Shoves window to the previous screen"""
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    """Shoves window to next screen"""
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)


def wide_screen(qtile):
    # TODO make it so I can check which state it's in
    home = os.path.expanduser('~')
    subprocess.call([home + '/.screenlayout/basic_setup.sh'])
    lazy.restart()


def split_screen(qtile):
    # TODO make it so I can check which state it's in
    home = os.path.expanduser('~')
    subprocess.call([home + '/.screenlayout/split_setup.sh'])
    lazy.restart()


keys = [
    # The essentials
    Key([alt, "control"],
        "t",
        lazy.spawn(my_terminal),
        desc='Launches My Terminal'),
    Key([mod],
        "r",
        lazy.spawn("dmenu_run -p 'Run: '"),
        desc='Dmenu Run Launcher'),
    Key([mod], "Tab", lazy.next_layout(), desc='Toggle through layouts'),
    Key([mod, "shift"], "c", lazy.window.kill(), desc='Kill active window'),
    Key([mod, "shift"], "Delete", lazy.window.kill(),
        desc='Kill active window'),
    Key([mod, "control"], "r", lazy.restart(), desc='Restart Qtile'),
    Key([mod, "shift"], "q", lazy.shutdown(), desc='Shutdown Qtile'),
    Key([mod],
        "q",
        lazy.spawn(
            "i3lock -e -n -c f8467d -i /home/kevinb/Pictures/wallpaper/bkgd.png"
        ),
        desc='Lock screen'),

    # Switch focus to specific monitor (out of three)
    Key([mod, alt], "h", lazy.to_screen(0),
        desc='Keyboard focus to ultra-wide'),
    Key([mod, alt], "k", lazy.to_screen(0),
        desc='Keyboard focus to ultra-wide'),
    Key([mod, alt], "l", lazy.to_screen(1), desc='Keyboard focus to laptop'),
    Key([mod, alt], "j", lazy.to_screen(2), desc='Keyboard focus to mini'),
    Key([mod, alt],
        "Left",
        lazy.to_screen(0),
        desc='Keyboard focus to ultra-wide'),
    Key([mod, alt],
        "Up",
        lazy.to_screen(0),
        desc='Keyboard focus to ultra-wide'),
    Key([mod, alt], "Right", lazy.to_screen(1),
        desc='Keyboard focus to laptop'),
    Key([mod, alt], "Down", lazy.to_screen(2), desc='Keyboard focus to mini'),

    # Switch focus of monitors
    Key([mod, alt],
        "bracketleft",
        lazy.next_screen(),
        desc='Move focus to next monitor'),
    Key([mod, alt],
        "bracketright",
        lazy.prev_screen(),
        desc='Move focus to prev monitor'),

    # Move to the adjacent screen
    Key([mod, "shift"], "bracketleft",
        lazy.function(window_to_previous_screen)),
    Key([mod, "shift"], "bracketright", lazy.function(window_to_next_screen)),

    # Window focus
    Key([mod], "Left", lazy.layout.left(), desc='Move focus left'),
    Key([mod], "Right", lazy.layout.right(), desc='Move focus right'),
    Key([mod], "Up", lazy.layout.up(), desc='Move focus up'),
    Key([mod], "Down", lazy.layout.down(), desc='Move focus right'),
    Key([mod], "h", lazy.layout.left(), desc='Move focus left'),
    Key([mod], "l", lazy.layout.right(), desc='Move focus right'),
    Key([mod], "k", lazy.layout.up(), desc='Move focus up'),
    Key([mod], "j", lazy.layout.down(), desc='Move focus right'),

    # Move windows
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        desc='Move windows down'),
    Key([mod, "shift"],
        "k",
        lazy.layout.shuffle_up(),
        desc='Move windows up in'),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc='Move windows up'),
    Key([mod, "shift"],
        "Down",
        lazy.layout.shuffle_down(),
        desc='Move windows down'),

    # Resize windows
    Key([mod, "control"],
        "h",
        lazy.layout.shrink(),
        lazy.layout.grow_left(),
        lazy.layout.increase_nmaster(),
        desc='Expand window, increase number in master pane'),
    Key([mod, "control"],
        "l",
        lazy.layout.grow(),
        lazy.layout.grow_right(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window, decrease number in master pane'),
    Key([mod, "control"],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        desc='Grow window to left. Grows monad'),
    Key([mod, "control"],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        desc='Grow window to right. Shrinks monad'),
    Key([mod],
        "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'),
    Key([mod],
        "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'),

    # Floating
    Key([mod, "shift"],
        "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'),
    Key([mod, "shift"],
        "m",
        lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'),

    # Stack controls
    Key([mod, "shift"],
        "space",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'),
    Key([mod],
        "space",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'),
    Key([mod, "control"],
        "Return",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'),

    # Media
    Key([], 'XF86AudioRaiseVolume',
        lazy.spawn('amixer -q -D pulse sset Master 5%+')),
    Key([], 'XF86AudioLowerVolume',
        lazy.spawn('amixer -q -D pulse sset Master 5%-')),
    Key([], 'XF86AudioMute',
        lazy.spawn('amixer -q -D pulse sset Master toggle')),
    Key([mod], 'F10', lazy.spawn('playerctl --player=spotifyd,%any previous')),
    Key([mod], 'F11',
        lazy.spawn('playerctl --player=spotifyd,%any play-pause')),
    Key([mod], 'F12', lazy.spawn('playerctl --player=spotifyd,%any next')),

    # Screenshots
    Key(["control"], "Print", lazy.spawn("flameshot gui"), desc='Screenshot'),

    # Launchers
    Key(["control", alt], "p", lazy.spawn("./pycharm"), desc='Pycharm'),
    Key(["control", alt], "v", lazy.spawn("code"), desc='VSCode'),
    Key([mod], "p", lazy.spawn("passmenu"), desc='pass'),
    Key(["control", alt], "y", lazy.spawn("yubioath-desktop"), desc='yubikey'),
    Key(["control", alt, "shift"],
        "y",
        lazy.spawn("killall yubioath-desktop "),
        desc='kill yubikey'),
    Key(["control", alt],
        "f",
        lazy.spawn("firefox --new-window"),
        desc='Firefox'),

    # Resolution
    Key([mod],
        "Scroll_Lock",
        lazy.function(wide_screen),
        desc='Set full width resolution'),
    Key([mod, 'shift'],
        "Scroll_Lock",
        lazy.function(split_screen),
        desc='Set split width resolution'),
]

mouse = [
    Drag([mod],
         "Button1",
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod],
         "Button3",
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
    Click([mod, "control", alt], "Button4", lazy.layout.grow(),
          lazy.layout.grow_right(), lazy.layout.decrease_nmaster()),
    Click([mod, "control"], "Button4", lazy.layout.grow_up(),
          lazy.layout.grow()),
    Click([mod, "control", alt], "Button5", lazy.layout.shrink(),
          lazy.layout.grow_left(), lazy.layout.increase_nmaster()),
    Click([mod, "control"], "Button5", lazy.layout.grow_down(),
          lazy.layout.shrink()),
]
